import { createState } from 'solid-js'
import { For } from 'solid-js/dom'

const GRID_SM = 5
const GRID_MD = 6
const GRID_LG = 7

export default function App () {
  const [state, setState] = createState({ step: 0, grid: GRID_SM })

  const add = (i) => {
    setState('step', (v) => v + 1)
    setState(i, state.step)
  }

  const clear = () => {
    setState('step', 0)
    Object.keys(state).forEach((k) => /\d+/.test(k) && setState(k, undefined))
  }

  const del = (i) => {
    setState('step', (v) => v - 1)
    setState(i, undefined)
  }

  const size = (n) => {
    setState('grid', Number(n))
  }

  return (
    <div class='sm:container mx-auto'>
      <div class='flex'>
        <div class='flex-1'>
          <button class='border rounded p-2 m-2 text-gray-600 font-mono' onClick={clear}>Clear</button>
        </div>

        <div class='flex-1 text-right'>
          <select onChange={(e) => size(e.target.value)} class='border rounded p-2 m-2 text-gray-600 font-mono text-center'>
            <For each={[GRID_SM, GRID_MD, GRID_LG]}>
              {(n) => (
                <option value={n}>{n} * {n}</option>
              )}
            </For>
          </select>
        </div>
      </div>

      <div
        class='grid'
        classList={{
          'grid-cols-5 grid-rows-5': state.grid === GRID_SM,
          'grid-cols-6 grid-rows-6': state.grid === GRID_MD,
          'grid-cols-7 grid-rows-7': state.grid === GRID_LG
        }}
      >
        <For each={Array(state.grid * state.grid)}>
          {(_, i) => (
            <div class='text-center text-gray-600' classList={{ 'border-t': i() >= state.grid, 'border-l': i() % state.grid > 0 }}>
              {state[i()] ? (
                <button onClick={[del, i()]} class='w-12 h-12 m-4 border text-blue-500 border-blue-500 font-mono'>
                  {state[i()] < 10 ? '0' + state[i()] : state[i()]}
                </button>
              ) : (
                <button onClick={[add, i()]} class='w-12 h-12 m-4 border text-gray-300'>
                  00
                </button>
              )}
            </div>
          )}
        </For>
      </div>
    </div>
  )
}
