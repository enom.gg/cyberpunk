import { render } from 'solid-js/web'

import './app.sass'
import App from './app'

render(() => (<App />), document.getElementById('root'))
